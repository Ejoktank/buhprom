let searcher = document.getElementById('search');
let list = document.getElementById('searchList');
let burgercontrol = document.getElementById('menu-toggle');

function createItem(ctx) {
    let li = document.createElement('li');
    li.classList.add('dropdown-list-item');

    let a = document.createElement('a');
    a.classList.add('dropdown-link');
    a.href = ctx.href;
    a.innerHTML = `${ctx.text} <small class="clarification">${ctx.desc}</small>`

    li.append(a);
    list.append(li);
}

function clearList() {
    list.innerHTML = '';
}

searcher.onkeyup = function(e) {
    let text = searcher.value;

    clearList();
    if(text.length < 1) return;

    let { res, ctx, desc } = search(text.toUpperCase().trim());
    // while(res.length > 5) res.pop();
    for(let i = 0; i < res.length; ++i)
        createItem(
            {
                href : "#" + res[i], 
                text : ctx[i], 
                desc : desc[i] ? desc[i] : ""
            });
}

function search(text) {
    let b = document.body;
    let res = [];
    let ctx = [];
    let desc = [];
    let get = function(e) {

        if(e.id === "search") return;
        if(e.id === "menu-box") return;
        
        if(!e.children.length && e.innerText && e.innerText.toUpperCase().trim().includes(text)) 
        {
            ctx.push(e.innerText);
            while(e && !e.hasAttribute('id')) e = e.parentElement;
            if(e) { res.push(e.id); desc.push(e.dataset.name); }
            else { ctx.pop(); }
            return;
        }

        if(e.children.length) 
            for(let g of e.children) get(g);        
    }

    get(b);
    return { res, ctx, desc };
}


document.getElementById('menuButton').onclick = (e) => {
    burgercontrol.checked = true;
}

document.getElementById('closeButton').onclick = (e) => {
    burgercontrol.checked = false;
}

document.getElementById('closeOverlay').onclick = (e) => {
    burgercontrol.checked = false;
}

